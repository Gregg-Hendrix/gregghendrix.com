const fetchButton = document.getElementById(`generate-users`);

fetchButton.addEventListener(`click`, (event) => {
  fetchButton.onclick = fetchUserz();
  fetchButton.onclick = "";
});

const clearData = document.getElementById(`erase-fetch`);

clearData.addEventListener(`click`, (event) => {
  const removeData = document.getElementById(`myTable`);

  removeData.remove();
});

const fetchUserz = async () => {
  const response = await fetch(`https://jsonplaceholder.typicode.com/users`);
  const data = await response.json();
  // console.log(data)

  const tableElement = document.createElement("table");
  let thName = document.createElement("th");
  let thEmail = document.createElement("th");
  thName.innerText = "Name";
  thEmail.innerText = "Email";
  tableElement.appendChild(thName);
  tableElement.appendChild(thEmail);

  for (let i = 0; i < data.length; i++) {
    let table = document.getElementById("table");

    let row = `<tr>
    <td>${data[i].name}</td>
    <td>${data[i].email}</td>
    </tr>`;

    table.innerHTML += row;
  }
};
