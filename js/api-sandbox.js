const fetchButton = document.getElementById('generate-users');

fetchButton.addEventListener('click', (event) => {
  fetchButton.onclick = fetchUser();
  document.getElementById('generate-users').disabled = true;
});

const clearData = document.getElementById('erase-fetch');

clearData.addEventListener('click', (event) => {
  const usersTable = document.getElementById('fetchTable');
  while (usersTable.childNodes.length > 0) {
    usersTable.removeChild(usersTable.lastChild);
  }
  document.getElementById('generate-users').removeAttribute('disabled');
});

const fetchUser = async () => {
  const response = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await response.json();

  for (let i = 0; i < data.length; i++) {
    let table = document.getElementById('fetchTable');

    let row = `<tr>
    <td>${data[i].name}</td>
    <td>${data[i].email}</td>
    </tr>`;

    table.innerHTML += row;
  }
};
